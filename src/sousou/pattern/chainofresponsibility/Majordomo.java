package sousou.pattern.chainofresponsibility;

public class Majordomo extends Manager {

	public Majordomo(String name) {
		super(name);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	@Override
	public void requestApplications(Request request) {
		//所能有的权限就是可准许下属两天内的假期
		if(request.getRequestType().equals("请假") && request.getNumber() <=5) {
			System.out.println(
					String.format("%s:%s 数量%d 被批准",
					this.name,
					request.getRequestContent(),
					request.getNumber()
					));
		}else {
			//其余的申请转交上级
			if(this.superior != null) {
				superior.requestApplications(request);
			}
		}

	}

}
