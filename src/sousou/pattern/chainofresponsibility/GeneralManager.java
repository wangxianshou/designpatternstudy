package sousou.pattern.chainofresponsibility;

public class GeneralManager extends Manager {

	public GeneralManager(String name) {
		super(name);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	@Override
	public void requestApplications(Request request) {
		//所能有的权限就是可准许下属两天内的假期
		if(request.getRequestType().equals("请假")) {
			System.out.println(
					String.format("%s:%s 数量%d 被批准",
					this.name,
					request.getRequestContent(),
					request.getNumber()
					));
		}else if(request.getRequestType().equals("加薪") && request.getNumber() <=500 ){
			System.out.println(
					String.format("%s:%s 数量%d 被批准",
					this.name,
					request.getRequestContent(),
					request.getNumber()
					));
		}else if(request.getRequestType().equals("加薪") && request.getNumber() > 500) {
			System.out.println(
					String.format("%s:%s 数量%d 再说吧",
					this.name,
					request.getRequestContent(),
					request.getNumber()
					));

		}

	}

}
