/**
 * 责任链模式
 * Avoid coupling the sender of a requestits receiver by giving more than one object a
 * chance to handle the request.Chain the receiving objects and pass the request along
 * the chain until anobject handles it.
 *
 * 为解除请求的发送者和接收者之间的耦合，而使多个对象都有机会处理这个请求。
 * 将这些对象连成一条链，并沿着这条链传递该请求，直到有对象处理它。
 *
 * 好处
 * 当客户提交一个请求时，请求是沿链传递直至有一个ConcreteHandler对象负责处理它
 * 这样就使得接收者和发送者都没有对方的明确信息，且链中的对象自己也并不知道链的结构。
 * 结果是职责链可简化对象的互相连接，它们仅需要保持一个指向后继者的引用，而不需要保持
 * 它所有的候选接收者的引用。
 * 注意
 * 一个请求极有可能到了链路的末端都得不到处理，或因为没有正确的配置而得不到处理
 */
/**
 * @author wangxianshou
 *
 */
package sousou.pattern.chainofresponsibility;