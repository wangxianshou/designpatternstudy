/**
 * 建造者模式
 * Separate the construction of a complex object from its representation so that the same
 * construction process can create different representations.
 *
 * 将一个复杂对象的构建与它的表示分离，使得同样的构建过程可以创建不同的表示。
 *
 * 建造小人的过程是稳定的，都需要头身手脚，而具体建造的细节是不同的，有高矮胖瘦
 * 将一个复杂对象的构建与它的表示分离，使得同样的构建过程可以创建不同的表示
 * 这个时候，需要应用于一个设计模式，就是建造者模式，也叫生成器模式。
 * 可以将一个产品的内部表象与产品的生成过程分割开来，从而可以使一个建造过程
 * 生成具有不同内部表象的产品对象。
 * 如果用了建造者模式，那么用户就只需要指定需要建造的类型就可以得到它们，
 * 而具体建造的过程和细节就不需要知道了。
 *
 * 建造模式中，一个很重要的类，指挥者（Director）用它来控制建造过程，也用它来
 * 隔离用户与建造过程的关联。
 *
 * 建造者模式是在当创建复杂对象的算法应该独立于该对象的组成部分以及它们的装配方式时
 * 适用的模式。
 */
/**
 * @author wangxianshou
 *
 */
package sousou.pattern.builder;