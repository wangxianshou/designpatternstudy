package sousou.pattern.builder;

/**
 * 画板【相当于c#自带的画板，请不要在意】
 * @author wangxianshou
 *
 */
public class Graphics {

	public void drawEllipse(Pen p,int arg1,int arg2,int arg3,int arg4) {
		System.out.println(String.format("Graphics.drawEllipse %d,%d,%d,%d", arg1,arg2,arg3,arg4));
	}

	public void drawRectangle(Pen p,int arg1,int arg2,int arg3,int arg4) {
		System.out.println(String.format("Graphics.drawRectangle %d,%d,%d,%d", arg1,arg2,arg3,arg4));
	}

	public void drawLine(Pen p,int arg1,int arg2,int arg3,int arg4) {
		System.out.println(String.format("Graphics.drawLine %d,%d,%d,%d", arg1,arg2,arg3,arg4));
	}

}
