package sousou.pattern.builder;

public class GoGoGo {

	public static void main(String[] args) {
		//画瘦小人
		Pen p1 = new Pen();
		Graphics g1 = new Graphics();
		PersonThinBuilder ptb = new PersonThinBuilder(g1,p1);
		new PersonDirector(ptb).createPerson();
		System.out.println("======================");
		//画胖小人
		Pen p2 = new Pen();
		Graphics g2 = new Graphics();
		PersonFatBuilder pfb = new PersonFatBuilder(g2,p2);
		new PersonDirector(pfb).createPerson();



	}

}
