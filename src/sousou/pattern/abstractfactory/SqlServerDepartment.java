package sousou.pattern.abstractfactory;

public class SqlServerDepartment implements IDepartment {

	@Override
	public void insert(Department department) {
		System.out.println("SqlServerDepartment.insert");

	}

	@Override
	public Department getDepartment(int id) {
		System.out.println(String.format("SqlServerDepartment.getDepartment by ID[%d]", id));
		return null;
	}

}
