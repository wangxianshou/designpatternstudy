package sousou.pattern.abstractfactory;

public class SqlServerUser implements IUser {

	@Override
	public void insert(User user) {
		System.out.println("SqlServerUser.insert");
	}

	@Override
	public User getUser(int id) {
		System.out.println(String.format("SqlServerUser.getUser by ID[%d]", id));
		return null;
	}

}
