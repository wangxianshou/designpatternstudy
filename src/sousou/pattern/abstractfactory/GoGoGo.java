package sousou.pattern.abstractfactory;

public class GoGoGo {

	public static void main(String[] args) {
		IFactory factory = null;
		//
		User user = new User();
		Department department = new Department();
		IUser iu = null;
		IDepartment idt = null;
		System.out.println("==User==========================");
		factory = new SqlServerFactory();
		iu = factory.createUser();
		iu.insert(user);
		iu.getUser(1);
		//
		factory = new AccessFactory();
		iu = factory.createUser();
		iu.insert(user);
		iu.getUser(1);
		//
		System.out.println("==Department==========================");
		factory = new SqlServerFactory();
		idt = factory.createDepartment();
		idt.insert(department);
		idt.getDepartment(1);
		//
		factory = new AccessFactory();
		idt = factory.createDepartment();
		idt.insert(department);
		idt.getDepartment(1);

	}

}
