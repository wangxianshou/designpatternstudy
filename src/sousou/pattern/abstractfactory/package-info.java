/**
 * 抽象工厂模式
 * Provide an interface for creating families of related or dependent object without
 * specifying their concrete classes.
 *
 * 提供一个创建一系列相关或相互依赖对象的接口，而无需指定它们的具体类。
 * 比较好的实现了 开放--封闭原则，依赖倒转原则
 *
 * 增加项目表Project，如何做
 * 添加三个类
 *   1.IProject
 *   2.SqlServerProject
 *   3.AccessProject
 * 还需要更改
 *   1.IFactory
 *   2.SqlServerFactory
 *   3.AccessFactory
 *
 *编程是门艺术，大批量的改动，显然是非常丑陋的做法。
 *可以用 简单工厂来改进抽象工厂
 */
/**
 * @author wangxianshou
 *
 */
package sousou.pattern.abstractfactory;