package sousou.pattern.abstractfactory;

public class AccessUser  implements IUser {

	@Override
	public void insert(User user) {
		System.out.println("AccessUser.insert");
	}

	@Override
	public User getUser(int id) {
		System.out.println(String.format("AccessUser.getUser by ID[%d]", id));
		return null;
	}
}
