package sousou.pattern.abstractfactory;

public interface IDepartment {
	void insert(Department department);
	Department getDepartment(int id);


}
