package sousou.pattern.abstractfactory;

public interface IFactory {

	IUser createUser();
	IDepartment createDepartment();

}
