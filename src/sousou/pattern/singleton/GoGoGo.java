package sousou.pattern.singleton;

public class GoGoGo {

	public static void main(String[] args) {
		Singleton s1_1 = Singleton.getInstance();
		Singleton s1_2 = Singleton.getInstance();
		if(s1_1.hashCode() == s1_2.hashCode()) {
			System.out.println(String.format("s1_1==s1_2 hashCode:%x", s1_1.hashCode()));
		}

		Singleton2 s2_1 = Singleton2.getInstance2();
		Singleton2 s2_2 = Singleton2.getInstance3();
		if(s2_1.hashCode() == s2_2.hashCode()) {
			System.out.println(String.format("s2_1==s2_2 hashCode:%x", s2_1.hashCode()));
		}

		Singleton3 s3_1 = Singleton3.getInstance();
		Singleton3 s3_2 = Singleton3.getInstance();
		if(s3_1.hashCode() == s3_2.hashCode()) {
			System.out.println(String.format("s3_1==s3_2 hashCode:%x", s3_1.hashCode()));
		}

	}

}
