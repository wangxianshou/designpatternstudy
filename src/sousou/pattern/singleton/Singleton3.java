package sousou.pattern.singleton;

public final class Singleton3 {

	private static final Singleton3 instance = new Singleton3();

	/**
	 * 构造方法不可见，这就堵死了外界利用new创建此类实例的可能
	 */
	private Singleton3() {

	}

	/**
	 * 此方法是获得本类实例的唯一全局访问点
	 * @return
	 */
	public static Singleton3 getInstance() {
		return instance;
	}

}
