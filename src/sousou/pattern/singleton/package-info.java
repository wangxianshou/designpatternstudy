/**
 * 单例模式
 * Ensure a class only has one instance,and provide a globe point of access to it.
 *
 * 保证一个类仅有一个对象，并提供一个访问它的全局访问点。
 * 让类自身负责保存它的唯一实例。这个类可以保证没有其他实例可以被创建，并且它可以提供一个访问该实例的方法。
 *
 * 实用类的静态方法的区别
 * 实用类不保存状态，仅仅提供一些静态方法或静态属性让外部使用
 * 不能用于继承多态
 * 单例模式
 * 是有状态的。虽然实例单一却是可以有子类来继承。
 */
/**
 * @author wangxianshou
 *
 */
package sousou.pattern.singleton;