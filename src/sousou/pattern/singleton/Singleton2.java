package sousou.pattern.singleton;

public class Singleton2 {

	private static Singleton2 instance;
	//内存上占用一个地址
	private static final Object syncRoot = new Object();

	/**
	 * 构造方法不可见，这就堵死了外界利用new创建此类实例的可能
	 */
	private Singleton2() {

	}

	/**
	 * 此方法是获得本类实例的唯一全局访问点
	 * @return
	 */
	public static Singleton2 getInstance() {
		synchronized(syncRoot) {
			if(instance == null) {
				instance = new Singleton2();
			}
		}
		return instance;
	}

	public synchronized static Singleton2 getInstance2() {
		if(instance == null) {
			instance = new Singleton2();
		}
		return instance;
	}

	/**
	 * 双重锁定 Double-Check Locking
	 * @return
	 */
	public static Singleton2 getInstance3() {
		if(instance == null) {
			synchronized(syncRoot) {
				if(instance == null) {
					instance = new Singleton2();
				}
			}
		}
		return instance;
	}

}
