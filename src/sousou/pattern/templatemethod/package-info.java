/**
 * 模板方法模式
 *
 * Define the skeleton of an algorithm inan operation, deferring some steps to subclasses.
 * Template Method lets subclasses redefine certain steps of an algorithm without
 * changing thealgorithm’s structure.
 *
 * 定义一个操作中的算法的骨架，而将一些步骤延迟到子类。TemplateMethod 使得子类可以不改变一个算法的结构即可重定义该算法的某些特定步骤。
 * 模板方法模式是通过把不变的行为搬移到超类，去除子类中的重复代码来体现它的优势。
 */
/**
 * @author wangxianshou
 *
 */
package sousou.pattern.templatemethod;