package sousou.pattern.templatemethod;

public class TestPaper {

	public void question1() {
		System.out.println("杨过得到，后来给了郭靖，炼成倚天剑和屠龙刀的玄铁可能是【】");
		System.out.println("a：球磨铸铁  b:马口铁 c:高速合金钢 d:碳素纤维");
		System.out.println(String.format("答案是：%s", answer1()));
	}

	public String answer1() {
		return "";
	}

	public void question2() {
		System.out.println("杨过，程英，陆无双铲除了情花，造成【】");
		System.out.println("a：这种植物不再害人  b：使一物种灭绝 c:破环生态平衡 d:沙漠化");
		System.out.println(String.format("答案是：%s", answer2()));
	}

	public String answer2() {
		return "";
	}

	public void question3() {
		System.out.println("蓝凤凰致使华山师徒呕吐不止，如果你是大夫，会开什么药【】");
		System.out.println("a：阿司匹林  b:牛黄解毒片 c:氟哌酸 d:大量喝牛奶");
		System.out.println(String.format("答案是：%s", answer3()));
	}

	public String answer3() {
		return "";
	}

}
