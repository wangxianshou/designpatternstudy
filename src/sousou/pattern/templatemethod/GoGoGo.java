package sousou.pattern.templatemethod;

public class GoGoGo {

	public static void main(String[] args) {
		System.out.println("学生甲抄的试卷");
		TestPaper studentA = new TestPaperA();
		studentA.question1();
		studentA.question2();
		studentA.question3();
		System.out.println("================");
		System.out.println("学生乙抄的试卷");
		TestPaper studentB = new TestPaperB();
		studentB.question1();
		studentB.question2();
		studentB.question3();

	}

}
