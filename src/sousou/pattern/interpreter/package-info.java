/**
 * 解释器模式
 * Given a language, define are presentation for its grammar along with an interpreter
 * that uses there presentation to interpret sentences in the language.
 *
 * 定义一个语言，定义它的文法的一种表示，并定义一个解释器，该解释器使用该表示来解释语言中的句子。
 *
 * 如果一种特定类型的问题发生的频率足够高，那么可能就值得将该问题的各个实例表述为一个简单语言中的句子。
 * 这样就可以构建一个解释器，该解释器通过解释这些句子来解决该问题。
 * 比如：正则表达式
 * 可以很容易地改变和扩展文法，因为该模式使用类来表示文法规则，可使用继承来改变或扩展该文法。
 * 也比较容易实现文法，因为定义抽象语法树中各个节点的类的实现大体类似，这些类都易于直接编写。
 */
/**
 * @author wangxianshou
 *
 */
package sousou.pattern.interpreter;