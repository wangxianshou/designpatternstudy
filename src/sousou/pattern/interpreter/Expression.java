package sousou.pattern.interpreter;

public abstract class Expression {

	public void interpret(PlayContext context) {
		if(context.getText().length() == 0) {
			return;
		}else {
			//ex: "O 3 E 0.5 G 0.5 A 3"
			String playKey = context.getText().substring(0,1);
			//
			context.setText(context.getText().substring(2));
			int idx = context.getText().indexOf(" ");
			double playValue = Double.parseDouble(context.getText().substring(0,idx));
			context.setText(context.getText().substring(idx+1));
			//
			System.out.println(String.format("key:%s value:%f", playKey,playValue));
			excute(playKey,playValue);

		}
	}

	public abstract void excute(String key,double value);

}
