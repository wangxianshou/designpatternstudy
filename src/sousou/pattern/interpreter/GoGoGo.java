package sousou.pattern.interpreter;

public class GoGoGo {

	public static void main(String[] args) {
		PlayContext context = new PlayContext();
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append("O 2");
		strBuilder.append(" E 0.5");
		strBuilder.append(" G 0.5");
		strBuilder.append(" A 3");
		strBuilder.append(" E 0.5");
		strBuilder.append(" G 0.5");
		strBuilder.append(" D 3");
		strBuilder.append(" E 0.5");
		strBuilder.append(" G 0.5");
		strBuilder.append(" A 0.5");
		strBuilder.append(" O 3");
		strBuilder.append(" C 1");
		strBuilder.append(" O 2");
		strBuilder.append(" A 0.5");
		strBuilder.append(" G 1");
		strBuilder.append(" C 0.5");
		strBuilder.append(" E 0.5");
		strBuilder.append(" D 3 ");
		context.setText(strBuilder.toString());
		System.out.println(String.format("context:%s", context.getText()));
		Expression expression = null;
		try {
			while(context.getText().length()>0) {
				String str = context.getText().substring(0,1);
				switch(str) {
				case "O":
					expression = new Scale();
					break;
				case "T"://音速追加，应用简单工厂+反射就可以做到不改动客户端了。
					//略
					break;
				default:
					expression = new Note();
					break;
				}
				expression.interpret(context);
			}

		}catch(Exception ex) {
			System.out.println(ex.getMessage());

		}
	}

}
