package sousou.pattern.interpreter;

public class Scale extends Expression {

	@Override
	public void excute(String key, double value) {
		String scale = "";
		int valInt = (int)value;

		switch(valInt) {
		case 1:
			scale = "低";
			break;
		case 2:
			scale = "中";
			break;
		case 3:
			scale = "高";
			break;
		}
		System.out.println(String.format("scale:%s", scale));

	}

}
