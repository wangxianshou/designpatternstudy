package sousou.pattern.abstractfactory3;

public class GoGoGo {

	public static void main(String[] args) {

		User user = new User();
		Department dept = new Department();

		IUser iu = DataAccess.createUser();
		iu.insert(user);
		iu.getUser(1);

		IDepartment idpt = DataAccess.createDepartment();
		idpt.insert(dept);
		idpt.getDepartment(1);

	}

}
