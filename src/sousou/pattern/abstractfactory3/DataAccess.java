package sousou.pattern.abstractfactory3;

import sousou.pattern.abstractfactory3.IDepartment;
import sousou.pattern.abstractfactory3.IUser;

public class DataAccess {
	private static final String pkg = "sousou.pattern.abstractfactory3";
	private static final String db = "SqlServer";
	//private static final String db = "Access";

	public static IUser createUser() {
		IUser result = null;
		try {
			result = (IUser)Class.forName(pkg + "." + db +"User").newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return result;
	}

	public static IDepartment createDepartment() {
		IDepartment result = null;
		try {
			result = (IDepartment)Class.forName(pkg + "." + db+"Department").newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return result;

	}


}
