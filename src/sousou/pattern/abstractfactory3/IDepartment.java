package sousou.pattern.abstractfactory3;

public interface IDepartment {
	void insert(Department department);
	Department getDepartment(int id);


}
