package sousou.pattern.abstractfactory3;

public interface IUser {

	void insert(User user);
	User getUser(int id);

}
