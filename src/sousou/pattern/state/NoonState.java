package sousou.pattern.state;

public class NoonState extends State {

	@Override
	public void writeProgram(Work w) {
		if(w.getHour()<13) {
			System.out.println(String.format("当前时间：%.0f点 饿了，午饭，犯困，午休", w.getHour()));
		}else {
			w.setState(new AfternoonState());
			w.writeProgram();
		}
	}

}
