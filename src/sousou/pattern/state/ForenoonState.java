package sousou.pattern.state;

public class ForenoonState extends State {

	@Override
	public void writeProgram(Work w) {
		if(w.getHour()<12) {
			System.out.println(String.format("当前时间：%.0f点 上午工作精神百倍", w.getHour()));
		}else {
			w.setState(new NoonState());
			w.writeProgram();
		}
	}

}
