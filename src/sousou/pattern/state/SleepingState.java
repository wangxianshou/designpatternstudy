package sousou.pattern.state;

public class SleepingState extends State {

	@Override
	public void writeProgram(Work w) {
		System.out.println(String.format("当前时间：%.0f点 不行了，睡着了", w.getHour()));
	}

}
