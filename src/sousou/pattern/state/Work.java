package sousou.pattern.state;

public class Work {


	public State getState() {
		return state;
	}
	public void setState(State state) {
		this.state = state;
	}
	private State state;

	public Work() {
		//工作初始化为上午工作状态，即上午9点开始上班
		state = new ForenoonState();
	}
	public double getHour() {
		return hour;
	}
	public void setHour(double hour) {
		this.hour = hour;
	}
	public boolean isFinish() {
		return finish;
	}
	public void setFinish(boolean finish) {
		this.finish = finish;
	}
	//钟点属性，状态转换的依据
	private double hour;
	//任务完成属性，是否能下班的依据
	private boolean finish = false;
	//
	public void writeProgram() {
		state.writeProgram(this);
	}

}
