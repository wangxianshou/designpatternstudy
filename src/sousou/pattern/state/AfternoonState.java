package sousou.pattern.state;

public class AfternoonState extends State {

	@Override
	public void writeProgram(Work w) {
		if(w.getHour()<17) {
			System.out.println(String.format("当前时间：%.0f点 下午状态还不错继续努力", w.getHour()));
		}else {
			w.setState(new EveningState());
			w.writeProgram();
		}
	}

}
