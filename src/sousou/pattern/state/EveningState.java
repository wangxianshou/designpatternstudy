package sousou.pattern.state;

public class EveningState extends State {

	@Override
	public void writeProgram(Work w) {
		if(w.isFinish()) {
			w.setState(new RestState());
			w.writeProgram();
		}else {
			if(w.getHour() < 21) {
				System.out.println(String.format("当前时间：%.0f点 加班疲惫至极", w.getHour()));
			}else {
				w.setState(new SleepingState());
				w.writeProgram();
			}
		}
	}

}
