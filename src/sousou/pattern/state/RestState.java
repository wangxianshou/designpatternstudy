package sousou.pattern.state;

public class RestState extends State {

	@Override
	public void writeProgram(Work w) {
		System.out.println(String.format("当前时间：%.0f点 下班回家了", w.getHour()));
	}

}
