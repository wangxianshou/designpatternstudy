package sousou.pattern.state;

public class GoGoGo {

	public static void main(String[] args) {
		//
		Work emergencyPJ = new Work();
		emergencyPJ.setHour(9);
		emergencyPJ.writeProgram();
		emergencyPJ.setHour(10);
		emergencyPJ.writeProgram();
		emergencyPJ.setHour(12);
		emergencyPJ.writeProgram();
		emergencyPJ.setHour(13);
		emergencyPJ.writeProgram();
		emergencyPJ.setHour(14);
		emergencyPJ.writeProgram();

		emergencyPJ.setHour(17);
		//emergencyPJ.setFinish(true);
		emergencyPJ.setFinish(false);
		emergencyPJ.writeProgram();

		emergencyPJ.setHour(19);
		emergencyPJ.writeProgram();
		emergencyPJ.setHour(22);
		emergencyPJ.writeProgram();

	}

}
