package sousou.pattern.prototype;

public class Resume implements Cloneable {
	private String name;
	private String sex;
	private String age;
	private WorkExperience work;//引用对象类型，也需要实现克隆（直到全部都是值类型）

	public Resume(String name) {
		this.name = name;
		this.work = new WorkExperience();
	}

	public Resume(WorkExperience work) {
		try {
			this.work = (WorkExperience)work.clone();
		} catch (CloneNotSupportedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}

	//设置个人信息
	public void setPersonalInfo(String sex,String age) {
		this.sex = sex;
		this.age = age;
	}
	//设置工作经历
	public void setWorkExperience(String workDate,String company) {
		this.work.setWorkDate(workDate);
		this.work.setCompany(company);
	}
	//显示
	public void display() {
		System.out.println(String.format("%s %s %s",name,sex,age));
		System.out.println(String.format("work:%s %s", work.getWorkDate(),work.getCompany()));

	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		Resume obj = new Resume(this.work);
		obj.name = this.name;
		obj.sex  = this.sex;
		obj.age  = this.age;
		return obj;

	}

}
