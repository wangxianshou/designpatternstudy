/**
 * 原型模式
 * Specify the kinds of objects to create using a prototypical instance, and create new
 * objects by copying the prototype.
 *
 * 用原型实例指定创建对象的种类，并且通过拷贝这个原型来创建新的对象。
 * 浅复制
 *            被复制对象的所有变量都含有与原来的对象相同的值，而所有的对其他对象的引用都仍然指向原来的对象
 * 深复制
 *            把引用对象的变量指向复制过的新对象，而不是原有的被引用的对象
 *
 */
/**
 * @author wangxianshou
 *
 */
package sousou.pattern.prototype;