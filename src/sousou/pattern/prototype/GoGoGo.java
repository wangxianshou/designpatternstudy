package sousou.pattern.prototype;

public class GoGoGo {

	public static void main(String[] args) {
		Resume a = new Resume("big bird");
		a.setPersonalInfo("male", "29");
		a.setWorkExperience("1998-2000", "X company");

		Resume b = null;
		try {
			b = (Resume)a.clone();
			b.setWorkExperience("2001-2010", "Y company");
		} catch (CloneNotSupportedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		Resume c = null;
		try {
			c = (Resume)a.clone();
			c.setWorkExperience("2011-2020", "Z company");
		} catch (CloneNotSupportedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		a.display();
		b.display();
		c.display();

	}

}
