package sousou.pattern.iterator;

public abstract class Aggregate {
	public abstract Iterator createIterator();

}
