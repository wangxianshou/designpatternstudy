package sousou.pattern.iterator;

import java.util.ArrayList;
import java.util.List;

public class ConcreteAggregate extends Aggregate {

	private List<Object> items = new ArrayList<>();
	@Override
	public Iterator createIterator() {
		// TODO 自動生成されたメソッド・スタブ
		return new ConcreteIterator(this);
	}

	public int size() {
		return items.size();
	}
	//java中没有C#的索引器 Object this[int index]
	public Object get(int index) {
		return items.get(index);
	}
	public Object add(Object object) {
		return items.add(object);
	}

}
