package sousou.pattern.iterator;

public class GoGoGo {

	public static void main(String[] args) {
		ConcreteAggregate a = new ConcreteAggregate();
		a.add("大鸟");
		a.add("小鸟");
		a.add("行李");
		a.add("带行李的乘客");
		a.add("老外");
		a.add("公交公司员工");
		a.add("小偷");

		Iterator i = new ConcreteIterator(a);
		Object item = i.first();
		while(!i.isDone()) {
			System.out.println(String.format("%s 请买车票", i.currentItem()));
			i.next();
		}


	}

}
