/**
 * 迭代器模式【为遍历不同的聚集结构提供如开始，下一个，是否结束，当前哪一个等统一的接口】
 * Provide a way to access the elements ofan aggregate object sequentially without
 * exposing its underlying representation.
 *
 * 提供一种方法顺序访问一个聚合对象中各个元素，而又不需暴露该对象的内部表示。
 *
 * 当你需要访问一个聚集对象，而且不管这些对象是什么都需要遍历的时候，考虑此模式
 * 需要对聚集对象有多种方式遍历时候，考虑此模式
 *
 * 现在其实用价值远不如学习价值大了，因为现在高级编程语言本身已经把这个模式在语言中了。
 *
 */
/**
 * @author wangxianshou
 *
 */
package sousou.pattern.iterator;