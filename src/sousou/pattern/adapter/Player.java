package sousou.pattern.adapter;

public abstract class Player {
	protected String name;

	public Player(String name) {
		this.name = name;
	}
	//进攻
	public abstract void attack();
	//防守
	public abstract void defense();

}
