package sousou.pattern.adapter;

public class Center extends Player {

	public Center(String name) {
		super(name);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	@Override
	public void attack() {
		System.out.println(String.format("Center[%s] attack", this.name));
	}

	@Override
	public void defense() {
		System.out.println(String.format("Center[%s] defense", this.name));

	}

}
