package sousou.pattern.adapter;

public class GoGoGo {

	public static void main(String[] args) {
		Player b = new Forwards("巴蒂尔");//英语
		b.attack();
		Player m = new Forwards("麦克格雷迪");//英语
		m.attack();
		Player ym = new Translator("姚明");//通过翻译，英语到中文
		ym.attack();
		ym.defense();

	}

}
