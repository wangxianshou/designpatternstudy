/**
 * 适配器模式【在双方都不太容易修改的时候，再使用适配器模式】
 * Convert the interface of a class into another interface clients except. Adapter lets
 * classes work together that couldn’t otherwise because of incompatible interfaces.
 *
 * 将一个类的接口转换成客户希望的另一个接口。Adapter模式使得原本由于接口不兼容而不能一起工作的那些了可以一起工作。
 *
 *在想使用一个已经存在的类，但如果它的接口，也就是它的方法和你的要求不相同时
 *两个类所做的事情相同或者类似，但具有不同的接口时要使用它
 *这样，客户端代码可以统一调用同一接口，使得更简单，更直接，更紧凑。
 *
 *公司内部，类和方法的命名应该有规范，最好前期就设计好，然后如果真的如你所说，接口不同时，
 *首先不应该考虑适配器，而是应该考虑通过重构统一接口。
 *
 *设计之初就考虑此模式呢？
 *比如公司设计一系统时，考虑使用第三方开发组件，而这个组件的接口与我们自己的系统接口是不相同的，
 *而我们也完全没有必要为了迎合它而改动自己的接口，此时尽管是在开发的设计阶段，也是可以考虑
 *用适配器模式来解决接口不同的问题。
 *
 */
/**
 * @author wangxianshou
 *
 */
package sousou.pattern.adapter;