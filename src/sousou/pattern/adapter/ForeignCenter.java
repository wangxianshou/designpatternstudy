package sousou.pattern.adapter;

/**
 * 外籍中锋
 * @author wangxianshou
 *
 */
public class ForeignCenter {

	private String name;
	public ForeignCenter(String name) {
		this.name = name;
	}

	//attack2:来代表自己语言的方式：例如中文叫进攻
	public void attack2() {
		System.out.println(String.format("外籍中锋[%s] 进攻", this.name));
	}

	//defense2:来代表自己语言的方式：例如中文叫防守
	public void defense2() {
		System.out.println(String.format("外籍中锋[%s] 防守", this.name));
	}

}
