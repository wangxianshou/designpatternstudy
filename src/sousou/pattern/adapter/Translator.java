package sousou.pattern.adapter;

public class Translator extends Player {

	private ForeignCenter wjzf ;

	public Translator(String name) {
		super(name);
		wjzf = new ForeignCenter(name);
	}

	@Override
	public void attack() {
		//翻译
		wjzf.attack2();

	}

	@Override
	public void defense() {
		//翻译
		wjzf.defense2();

	}

}
