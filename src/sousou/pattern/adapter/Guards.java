package sousou.pattern.adapter;

public class Guards extends Player {

	public Guards(String name) {
		super(name);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	@Override
	public void attack() {
		System.out.println(String.format("Guards[%s] attack", this.name));
	}

	@Override
	public void defense() {
		System.out.println(String.format("Guards[%s] defense", this.name));

	}

}
