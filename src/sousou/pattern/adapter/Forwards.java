package sousou.pattern.adapter;

public class Forwards extends Player {

	public Forwards(String name) {
		super(name);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	@Override
	public void attack() {
		System.out.println(String.format("Forwards[%s] attack", this.name));
	}

	@Override
	public void defense() {
		System.out.println(String.format("Forwards[%s] defense", this.name));

	}

}
