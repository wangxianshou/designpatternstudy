/**
 * 中介者模式
 * Define an object that encapsulates howa set of objects interact. Mediator promotes
 * loose coupling by keeping objectsfrom referring to each other explicitly and it lets
 * you vary their interactionindependently.
 *
 * 用一个中介对象来封装一系列的对象交互。中介者使各对象不需要显示地相互引用，从而使其耦合松散，而且可以独立地改变它们之间的交互。
 *
 *中介者模式很容易在系统中应用，也很容易在系统中误用。
 *当系统中出现了 多对多 交互复杂的对象群时，不要急于使用中介者模式，而是要先反思系统在设计上是不是合理。
 *
 *由于ConcreteMediator控制了集中化，于是就把交互复杂性变为了中介者的复杂性，这就使得中介者会变得
 *比任何一个ConcreteCollague都复杂。
 *
 */
/**
 * @author wangxianshou
 *
 */
package sousou.pattern.mediator;