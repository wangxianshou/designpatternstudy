package sousou.pattern.mediator;

public class GoGoGo {

	public static void main(String[] args) {
		ConcreteMediator m = new ConcreteMediator();
		ConcreteColleague1 c1 = new ConcreteColleague1(m);
		ConcreteColleague2 c2 = new ConcreteColleague2(m);
		m.setColleague1(c1);
		m.setColleague2(c2);
		c1.send("how are you ? ");
		//内部可以搭配适配器模式
		c2.send("我很好，你呢");

	}

}
