package sousou.pattern.mediator;

public class ConcreteColleague1 extends Colleague {

	public ConcreteColleague1(Mediator mediator) {
		super(mediator);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	@Override
	public void send(String message) {
		System.out.println(String.format("同事1说：%s", message));
		this.mediator.send(message, this);

	}

	@Override
	public void recieve(String message) {
		System.out.println(String.format("同事1得到消息：%s", message));

	}
}
