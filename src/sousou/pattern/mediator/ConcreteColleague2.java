package sousou.pattern.mediator;

public class ConcreteColleague2 extends Colleague {

	public ConcreteColleague2(Mediator mediator) {
		super(mediator);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	@Override
	public void send(String message) {
		System.out.println(String.format("同事2说：%s", message));
		this.mediator.send(message, this);

	}

	@Override
	public void recieve(String message) {
		System.out.println(String.format("同事2得到消息：%s", message));

	}
}
