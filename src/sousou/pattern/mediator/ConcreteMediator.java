package sousou.pattern.mediator;
/**
 * 具体中介者
 * 减少了ConcreteColleague类之间的耦合，
 * 但这又使得ConcreteMediator责任太多了，如果这里出了问题则整个系统都会有问题
 * @author wangxianshou
 *
 */
public class ConcreteMediator extends Mediator {

	public ConcreteColleague1 getColleague1() {
		return colleague1;
	}

	public void setColleague1(ConcreteColleague1 colleague1) {
		this.colleague1 = colleague1;
	}

	public ConcreteColleague2 getColleague2() {
		return colleague2;
	}

	public void setColleague2(ConcreteColleague2 colleague2) {
		this.colleague2 = colleague2;
	}

	private ConcreteColleague1 colleague1;
	private ConcreteColleague2 colleague2;

	@Override
	public void send(String message, Colleague colleague) {
		if(colleague1 == colleague) {
			colleague2.recieve(message);
		}else {
			colleague1.recieve(message);
		}

	}

}
