package sousou.pattern.mediator;
/**
 * 	抽象中介者
 * @author wangxianshou
 *
 */
public abstract class Mediator {
	public abstract void send(String message,Colleague colleague);

}
