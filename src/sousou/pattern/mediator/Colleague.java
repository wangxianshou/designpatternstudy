package sousou.pattern.mediator;
/**
 * 抽象同事类
 * @author wangxianshou
 *
 */
public abstract class Colleague {
	protected Mediator mediator;
	public Colleague(Mediator mediator) {
		this.mediator = mediator;
	}

	public abstract void send(String message);
	public abstract void recieve(String message);

}
