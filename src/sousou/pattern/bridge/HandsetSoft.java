package sousou.pattern.bridge;
/**
 * 手机软件抽象类（视点1）
 * @author wangxianshou
 *
 */
public abstract class HandsetSoft {
	protected String name;
	public HandsetSoft(String name) {
		this.name = name;
	}
	public abstract void run();

}
