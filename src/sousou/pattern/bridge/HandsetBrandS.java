package sousou.pattern.bridge;

public class HandsetBrandS extends HandsetBrand {

	public HandsetBrandS(String name) {
		super(name);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	@Override
	public void run() {
		System.out.println(String.format("Brand:%s", this.name));
		soft.run();
	}

}
