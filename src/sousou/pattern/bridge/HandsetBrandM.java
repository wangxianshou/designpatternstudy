package sousou.pattern.bridge;

public class HandsetBrandM extends HandsetBrand {

	public HandsetBrandM(String name) {
		super(name);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	@Override
	public void run() {
		System.out.println(String.format("Brand:%s", this.name));
		soft.run();
	}

}
