package sousou.pattern.bridge;

public class GoGoGo {

	public static void main(String[] args) {
		//小米
		HandsetBrand xiaomi = new HandsetBrandN("XIAOMI");
		xiaomi.setHandsetSoft(new HandsetGame("手机游戏"));//简单工厂模式
		xiaomi.run();
		xiaomi.setHandsetSoft(new HandsetAddressList("手机通讯录"));//简单工厂模式
		xiaomi.run();
		//.... 加入其他软件
		//华为
		HandsetBrand huawei = new HandsetBrandM("HUAWEI");
		huawei.setHandsetSoft(new HandsetGame("手机游戏"));
		huawei.run();
		huawei.setHandsetSoft(new HandsetAddressList("手机通讯录"));
		huawei.run();
		//VIVO
		HandsetBrand vivo = new HandsetBrandM("VIVO");
		vivo.setHandsetSoft(new HandsetMP3("MP3"));
		vivo.run();
		//iphone
		//加入其他品牌


	}

}
