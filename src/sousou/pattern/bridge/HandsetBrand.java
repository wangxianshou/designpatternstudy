package sousou.pattern.bridge;

/**
 * 手机品牌抽象类（视点2）
 * @author wangxianshou
 *
 */
public abstract class HandsetBrand {
	protected HandsetSoft soft;
	protected String name;
	public HandsetBrand(String name) {
		this.name = name;
	}
	/**
	 *设置手机软件
	 *品牌需要关注软件，所以可在机器中安装软件，以备运行。
	 * @param soft
	 */
	public void setHandsetSoft(HandsetSoft soft) {
		this.soft = soft;
	}

	public abstract void run();

}
