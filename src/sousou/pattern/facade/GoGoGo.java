package sousou.pattern.facade;

public class GoGoGo {

	public static void main(String[] args) {
		Facade facade = new Facade();

		facade.doSomeThingA();
		facade.doSomeThingB();

	}

}
