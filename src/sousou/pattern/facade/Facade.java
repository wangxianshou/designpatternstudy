package sousou.pattern.facade;

public class Facade {

	private SubSystemOne one;
	private SubSystemTwo two;
	private SubSystemThree three;
	private SubSystemFour four;

	public Facade() {
		one = new SubSystemOne();
		two = new SubSystemTwo();
		three = new SubSystemThree();
		four = new SubSystemFour();
	}

	public void doSomeThingA() {
		System.out.println("+ Facade.doSomeThingA");
		one.methodOne();
		two.methodTwo();
		four.methodFour();
	}

	public void doSomeThingB() {
		System.out.println("+ Facade.doSomeThingB");
		two.methodTwo();
		three.methodThree();
	}

}
