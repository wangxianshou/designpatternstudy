/**
 * 装饰模式
 * Attach additional responsibilities to an object dynamically. Decorators provides a
 * flexible alternative to subclasses for extending functionality.
 *
 * 动态地给一个对象添加一些额外的职责。就扩展功能而言，Decorator模式比生成子类的方式更为灵活。
 *
 * 为已有功能动态地添加更多功能的一种方式
 * 把每个要装饰的功能放在单独的类中，并让这个类包装它所要装饰的对象，
 * 因此，当需要执行特殊行为时，客户代码就可以在运行时
 * 根据需要有选择地，按顺序地使用装饰功能包装对象
 * 优点总结：
 * 把类中的装饰功能从类中搬移去除，可以简化原有的类。
 * 有效地把类的核心职责和装饰功能区分开了，而且可以去除相关类中重复的装饰逻辑。
 * 注意：【装饰模式的顺序很重要】
 * 例如：加密数据和过滤词汇都可以是数据持久化前的装饰功能，但若先加密数据再过滤词汇就会出问题，
 * 最理想的情况，是保证装饰类之间彼此独立，这样它们就可以以任意的顺序进行组合了。
 */
/**
 * @author wangxianshou
 *
 */
package sousou.pattern.decorator;