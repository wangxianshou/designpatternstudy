package sousou.pattern.decorator;

public class Person {

	public Person() {

	}

	private String name;
	public Person(String name) {
		this.name = name;
	}

	public void show() {
		System.out.print(String.format("装扮的%s", name));
	}
}
