package sousou.pattern.proxy;

public class Pursuit implements IGiveGift {

	private SchoolGirl mm;
	public SchoolGirl getMm() {
		return mm;
	}

	public void setMm(SchoolGirl mm) {
		this.mm = mm;
	}

	private String name;

	public Pursuit() {
	}

	public Pursuit(SchoolGirl mm) {
		this.mm = mm;
	}

	@Override
	public void giveDolls() {
		System.out.println(String.format("gg:%s->mm:%s [%s]",this.name, mm.getName(),"giveDolls"));

	}

	@Override
	public void giveFlowers() {
		System.out.println(String.format("gg:%s->mm:%s [%s]",this.name, mm.getName(),"giveFlowers"));

	}

	@Override
	public void giveChocolate() {
		System.out.println(String.format("gg:%s->mm:%s [%s]",this.name, mm.getName(),"giveChocolate"));

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
