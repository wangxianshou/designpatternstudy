package sousou.pattern.proxy;

public class GoGoGo {

	public static void main(String[] args) {

		System.out.println("==书中例子========================");
		SchoolGirl mm = new SchoolGirl();
		mm.setName("Girl");
		Proxy proxy = new Proxy(mm);
		proxy.giveDolls();
		proxy.giveFlowers();
		proxy.giveChocolate();
		//
		System.out.println("==我自己的改动==========================");
		SchoolGirl mm2 = new SchoolGirl();
		mm2.setName("Girl2");
		//
		Pursuit gg = new Pursuit();
		gg.setName("Boy2");
		//Girl和Boy不认识，交给代理来传话
		Proxy proxy2 = new Proxy(mm2,gg);
		proxy2.giveDolls();
		proxy2.giveFlowers();
		proxy2.giveChocolate();

	}

}
