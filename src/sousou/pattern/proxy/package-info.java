/**
 * 代理模式
 * Provide a surrogate or placeholder foranther object to control access to it.
 *
 * 为其他对象提供一个代理以控制对这个对象的访问。
 */
/**
 * @author wangxianshou
 *
 */
package sousou.pattern.proxy;