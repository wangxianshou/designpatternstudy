package sousou.pattern.proxy;

public class Proxy implements IGiveGift {

	private Pursuit gg;
	private SchoolGirl mm;
	public Proxy(SchoolGirl mm) {
		this.gg = new Pursuit(mm);
		this.gg.setName("Boy");
	}

	public Proxy(SchoolGirl mm,Pursuit gg) {
		this.gg = gg;
		this.mm = mm;
		this.gg.setMm(this.mm);
	}

	@Override
	public void giveDolls() {
		gg.giveDolls();

	}

	@Override
	public void giveFlowers() {
		gg.giveFlowers();

	}

	@Override
	public void giveChocolate() {
		gg.giveChocolate();

	}

}
