package sousou.pattern.proxy;

public interface IGiveGift {
	void giveDolls();
	void giveFlowers();
	void giveChocolate();

}
