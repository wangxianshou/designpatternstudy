package sousou.pattern.composite;

import java.util.Collections;

public class HRDepartment extends Company {

	public HRDepartment(String name) {
		super(name);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	@Override
	public void add(Company c) {
		// TODO 自動生成されたメソッド・スタブ

	}

	@Override
	public void remove(Company c) {
		// TODO 自動生成されたメソッド・スタブ

	}

	@Override
	public void display(int depth) {
		System.out.println(String.join("", Collections.nCopies(depth, "-")) + " " + this.name);

	}

	@Override
	public void lineOfDuty() {
		System.out.println(String.format("%s 员工招聘培训管理", this.name));

	}

}
