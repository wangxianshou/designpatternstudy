package sousou.pattern.composite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 具体公司类 实现接口 树枝节点
 * @author wangxianshou
 *
 */
public class ConcreteCompany extends Company {

	private List<Company> children = new ArrayList<>();

	public ConcreteCompany(String name) {
		super(name);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	@Override
	public void add(Company c) {
		children.add(c);

	}

	@Override
	public void remove(Company c) {
		children.remove(c);

	}

	@Override
	public void display(int depth) {
		System.out.println(String.join("", Collections.nCopies(depth, "-")) + " " + this.name);
		for(Company it : children) {
			it.display(depth+2);
		}
	}

	@Override
	public void lineOfDuty() {
		for(Company it : children) {
			it.lineOfDuty();
		}
	}

}
