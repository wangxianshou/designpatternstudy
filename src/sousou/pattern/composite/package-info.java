/**
 * 组合模式【整体与部分可以被一致对待】
 * Compose object into tree structures torepresent part-whole hierarchy. Composite lets
 * clients treat individual objectsand compositions of objects uniformly.
 *
 * 将对象组合成树形结构以表示“部分-整体”的层次结构。Composite 使得客户对单个对象和复合对象的使用具有一致性。
 *
 * 发现需求中是体现部分与整体层次的结构时，以及你希望用户可以忽略组合对象与单个对象的不同，统一地使用组合结构中
 * 的所有对象时，考虑组合模式。
 * 透明方式：叶节点和枝节点对于外界没有区别，它们具备完全一致的行为接口，问题也很明显：叶节点不具备Add Remove功能
 *安全方式：树叶和树枝类将 不具有相同的接口，客户端的调用需要做相应的判断带来了不便。
 *
 *基本对象可以被组合成更复杂的对象，而这个组合对象又可以被组合，这样不断地递归下去，客户代码中，
 *任何用到基本对象的地方都可以使用组合对象。
 *用户是不用关心到底是处理一个叶节点还是处理一个组合组件，也就用不着为定义组合而写一些选择判断语句了。
 */
/**
 * @author wangxianshou
 *
 */
package sousou.pattern.composite;