package sousou.pattern.composite;

public class GoGoGo {

	public static void main(String[] args) {
		ConcreteCompany root = new ConcreteCompany("北京总公司");
		root.add(new HRDepartment("总公司人力资源部"));
		root.add(new FinanceDepartment("总公司财务部"));

		ConcreteCompany comp1 = new ConcreteCompany("上海华东分公司");
		comp1.add(new HRDepartment("上海华东分公司人力资源部"));
		comp1.add(new FinanceDepartment("上海华东分公司财务部"));
		root.add(comp1);

		ConcreteCompany comp1_1 = new ConcreteCompany("南京办事处");
		comp1_1.add(new HRDepartment("南京办事处人力资源部"));
		comp1_1.add(new FinanceDepartment("南京办事处财务部"));
		comp1.add(comp1_1);
		ConcreteCompany comp1_2 = new ConcreteCompany("杭州办事处");
		comp1_2.add(new HRDepartment("杭州办事处人力资源部"));
		comp1_2.add(new FinanceDepartment("杭州办事处财务部"));
		comp1.add(comp1_2);

		ConcreteCompany comp2 = new ConcreteCompany("日本分公司");
		comp2.add(new HRDepartment("日本分公司人力资源部"));
		comp2.add(new FinanceDepartment("日本分公司财务部"));
		root.add(comp2);

		System.out.println("组织结构图");
		root.display(1);
		System.out.println("职责");
		root.lineOfDuty();


	}

}
