package sousou.pattern.composite;

import java.util.Collections;

public class FinanceDepartment extends Company {

	public FinanceDepartment(String name) {
		super(name);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	@Override
	public void add(Company c) {
		// TODO 自動生成されたメソッド・スタブ

	}

	@Override
	public void remove(Company c) {
		// TODO 自動生成されたメソッド・スタブ

	}

	@Override
	public void display(int depth) {
		System.out.println(String.join("", Collections.nCopies(depth, "-")) + " " + this.name);

	}

	@Override
	public void lineOfDuty() {
		System.out.println(String.format("%s 公司财务收支管理", this.name));

	}

}
