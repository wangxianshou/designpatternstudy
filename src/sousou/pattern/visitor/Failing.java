package sousou.pattern.visitor;

public class Failing extends Action {

	@Override
	public void getManConclusion(Man concreteElementA) {
		System.out.println(String.format("%-30s when %-30s时，我坚强我不哭",concreteElementA.getClass().getName(),this.getClass().getName()));

	}

	@Override
	public void getWomanConclusion(Woman concreteElementB) {
		System.out.println(String.format("%-30s when %-30s时，男人没一个好东西",concreteElementB.getClass().getName(),this.getClass().getName()));
	}


}
