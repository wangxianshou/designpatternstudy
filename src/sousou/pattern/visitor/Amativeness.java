package sousou.pattern.visitor;

public class Amativeness extends Action {

	@Override
	public void getManConclusion(Man concreteElementA) {
		System.out.println(String.format("%-30s when %-30s时，你好漂亮",concreteElementA.getClass().getName(),this.getClass().getName()));

	}

	@Override
	public void getWomanConclusion(Woman concreteElementB) {
		System.out.println(String.format("%-30s when %-30s时，你好帅气",concreteElementB.getClass().getName(),this.getClass().getName()));
	}


}
