package sousou.pattern.visitor;

public class Success extends Action {

	@Override
	public void getManConclusion(Man concreteElementA) {
		System.out.println(String.format("%-30s when %-30s时，背后多半有一个伟大的女人。",concreteElementA.getClass().getName(),this.getClass().getName()));

	}

	@Override
	public void getWomanConclusion(Woman concreteElementB) {
		System.out.println(String.format("%-30s when %-30s时，背后大多有一个不成功的男人。",concreteElementB.getClass().getName(),this.getClass().getName()));
	}

}
