package sousou.pattern.visitor;

public class Marriage extends Action {

	@Override
	public void getManConclusion(Man concreteElementA) {
		System.out.println(String.format("%-30s when %-30s时，感慨道：恋爱游戏终结，有妻徒刑遥遥无期",concreteElementA.getClass().getName(),this.getClass().getName()));

	}

	@Override
	public void getWomanConclusion(Woman concreteElementB) {
		System.out.println(String.format("%-30s when %-30s时，欣慰到：爱情长跑漫漫，婚姻保险保平安。",concreteElementB.getClass().getName(),this.getClass().getName()));
	}


}
