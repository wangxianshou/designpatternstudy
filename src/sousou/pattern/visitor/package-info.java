/**
 * 访问者模式
 * Represent an operation to be performedon the elements of an object structure. Visitor
 * lets you define a new operation without changing the classes of the elements on which
 * it operates.
 *
 * 表示一个作用于某对象结构中的各元素的操作。它使你可以在不改变各元素类别的前提下定义作用于这些元素的新操作。
 *
 * 访问者模式适用于数据结构相对稳定的系统
 * 它把数据结构和作用于结构上的操作之间的耦合解脱开，使得操作集合可以相对自由地演化
 * 访问者模式的目的是要把处理从数据中分离出来。
 * 有比较稳定的数据结构，又有易于变化的算法的话，使用访问者模式就是比较合适的，
 * 因为访问者模式使得算法操作的增加变得容易。
 * 访问者模式的优点就是增加新的操作很容易，因为增加新的操作就意味着增加一个新的访问者。
 * 访问者模式将有关的行为集中到一个访问者对象中。
 * 那访问者的缺点其实也就是使增加新的数据结构变得困难。
 * 访问者模式的能力和复杂性是把双刃剑，只有当你真正需要它的时候，才考虑使用它。
 *
 */
/**
 * @author wangxianshou
 *
 */
package sousou.pattern.visitor;