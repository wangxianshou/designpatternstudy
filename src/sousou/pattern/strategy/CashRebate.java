package sousou.pattern.strategy;

public class CashRebate extends CashSuper {

	private double moneyRebate = 1d;
	public CashRebate(String moneyRebate) {
		this.moneyRebate = Double.parseDouble(moneyRebate);

	}

	@Override
	public double acceptCash(double money) {
		// 打折
		return money * moneyRebate;
	}

}
