package sousou.pattern.strategy;

public class CashContext {

	private CashSuper cs;
	public CashSuper getCs() {
		return cs;
	}

	public void setCs(CashSuper cs) {
		this.cs = cs;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	private String type;
	public CashContext(CashSuper csuper) {
		this.cs = csuper;
	}

	public CashContext(String type) {
		this.type = type;
		switch(type) {
		case "正常收费":
			cs = new CashNormal();
			break;
		case "满300 返100":
			cs = new CashReturn("300", "100");
			break;
		case "打8折":
			cs = new CashRebate("0.8");
			break;
		}
	}

	public double getResult(double money) {
		return cs.acceptCash(money);
	}

}
