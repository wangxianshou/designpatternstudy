/**
 * 策略模式
 * Define a family of algorithms,encapsulate each one and make them interchangeable.
 * Strategy lets the algorithmvary independently from clients that use it.
 *
 * 定义一系列的算法，把它们一个个封装起来，并且使他们可相互替换。本模式使得算法的变化可以独立于使用它的客户。
 * 策略模式是一种定义一系列算法的方法，从概念上来看，所有这些算法完成的都是相同的工作，只是实现不同。
 * 它可以以相同的方式调用所有的算法，减少了各种算法类与使用算法类之间的耦合。
 *
 * 策略模式的优点是简化了单元测试，因为每个算法都有自己的类，可以通过自己的接口单独测试。
 *
 * 当不同的行为堆在一个类中时候，就很难避免使用条件语句来选择合适的行为。将这些行为封装在一个个对立的类中
 * 可以在使用这些行为的类中消除条件语句。
 * 策略模式是用来封装算法的，但在实践中，我们发现可以用它来封装几乎任何类型的规则，只要在分析中听到
 * 【需要在不同时间应用不同的业务规格】就可以考虑使用策略模式处理这种变化的可能性。
 *
 * 在策略模式中，选择所用具体实现的职责由客户端承担，并转给策略模式的Context对象。
 *
 *简单工厂模式
 *CashSuper csuper = CashFactory.createCashAccept(区分);
 *csuper.getResult();
 *策略模式与简单工厂结合
 *CashContext csuper = new CashContext(区分)
 *csuper.getResult();
 */
/**
 * @author wangxianshou
 *
 */
package sousou.pattern.strategy;