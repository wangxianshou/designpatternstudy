package sousou.pattern.strategy;

public class GoGoGo {

	public static void main(String[] args) {

		CashContext cashContext = null;

		cashContext = new CashContext("正常收费");
		System.out.println(String.format("%-20s > Result:%f", cashContext.getType(),cashContext.getResult(300)));
		cashContext = new CashContext("满300 返100");
		System.out.println(String.format("%-10s > Result:%f", cashContext.getType(),cashContext.getResult(300)));
		cashContext = new CashContext("打8折");
		System.out.println(String.format("%-24s > Result:%f", cashContext.getType(),cashContext.getResult(300)));


	}

}
