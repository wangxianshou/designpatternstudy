package sousou.pattern.strategy;

public abstract class CashSuper {

	public abstract double acceptCash(double money);

}
