package sousou.pattern.memento;

public class GoGoGo {

	public static void main(String[] args) {
		//大战Boss前
		System.out.println(">>>>大战Boss前");
		GameRole quanhuang  = new GameRole();
		quanhuang.getInitState();
		quanhuang.displayState();
		//保存进度
		System.out.println(">>>>保存进度");
		RoleStateCaretaker stateAdmin = new RoleStateCaretaker();
		stateAdmin.setMemento(quanhuang.saveState());;
		//大战Boss时，损耗严重，团灭。。。
		System.out.println(">>>>大战Boss时，损耗严重，团灭。。。");
		quanhuang.fightBoss();
		quanhuang.displayState();
		//恢复存档再闯关
		System.out.println(">>>>恢复存档再闯关");
		quanhuang.recoveryState(stateAdmin.getMemento());
		quanhuang.displayState();

	}

}
