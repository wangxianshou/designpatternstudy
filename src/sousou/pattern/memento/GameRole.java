package sousou.pattern.memento;

public class GameRole {
	public int getVitality() {
		return vitality;
	}
	public void setVitality(int vitality) {
		this.vitality = vitality;
	}
	public int getAttack() {
		return attack;
	}
	public void setAttack(int attack) {
		this.attack = attack;
	}
	public int getDefense() {
		return defense;
	}
	public void setDefense(int defense) {
		this.defense = defense;
	}

	private int vitality;
	private int attack;
	private int defense;
	/**
	 * 状态显示
	 */
	public void displayState() {
		System.out.println(String.format("角色当前状态："));
		System.out.println(String.format("生命值：%d",this.vitality));
		System.out.println(String.format("攻击力：%d",this.attack));
		System.out.println(String.format("防御力：%d",this.defense));

	}
	/**
	 * 获得初始状态
	 */
	public void getInitState() {
		this.vitality = 100;
		this.attack   = 100;
		this.defense  = 100;

	}
	/**
	 * 打Boss，战斗后
	 */
	public void fightBoss() {
		this.vitality = 0;
		this.attack   = 0;
		this.defense  = 0;

	}

	/**
	 * 保存角色状态
	 * 将游戏角色的三个状态值通过实例化“角色状态存储箱”返回
	 */
	public RoleStateMemento saveState() {
		return new RoleStateMemento(this.vitality,this.attack,this.defense);
	}

	/**
	 * 恢复角色状态
	 * 可以将外部的“角色状态存储箱”中的状态值恢复
	 * @param memento
	 */
	public void recoveryState(RoleStateMemento memento) {
		this.vitality = memento.getVit();
		this.attack   = memento.getAtk();
		this.defense  = memento.getDef();

	}

}
