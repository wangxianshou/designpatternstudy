package sousou.pattern.memento;
/**
 * 角色状态管理者
 * @author wangxianshou
 *
 */
public class RoleStateCaretaker {

	public RoleStateMemento getMemento() {
		return memento;
	}

	public void setMemento(RoleStateMemento memento) {
		this.memento = memento;
	}

	private RoleStateMemento memento;



}
