/**
 * 备忘录模式【如果数据很大很多，那么在资源消耗上，会非常消耗内存】
 * Without violating encapsulates, captureand externalize an object’s internal state so
 * that the object can be restored to this state later.
 *
 * 在不破坏封装性的前提下，捕获一个对象的内部状态，并在该对象之外保持该状态，这样以后就可以将该对象恢复到保存的状态。
 *
 * 比较适用于功能比较复杂的，但需要维护或记录属性历史的类，或者需要保存的属性只是众多属性中的一小部分时，
 * Originator（发起人类）可以根据保存的Memento（备忘录类）信息还原到前一状态。
 * 如果在某个系统中使用命令模式时，需要实现命令的撤销功能，那么命令模式可以使用备忘录模式来存储可撤销操作的状态。
 *有时一些对象的内部信息必须保存在对象以外的地方，但是必须要由对象自己读取，
 *这时，使用备忘录可以把复杂的对象内部信息对其他的对象屏蔽起来，从而可以恰当地保持封装的边界。
 *最大作用
 *当角色的状态改变的时候，有可能这个状态无效，这时候就可以使用暂时存储起来的备忘录将状态复原。
 *
 */
/**
 * @author wangxianshou
 *
 */
package sousou.pattern.memento;