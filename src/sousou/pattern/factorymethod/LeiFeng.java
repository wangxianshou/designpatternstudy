package sousou.pattern.factorymethod;

/**
 * LeiFeng也可以通过接口的方式，具体要做的事情就转移到各个实现子类中去了
 * 例如
 * 问候
 *      中国语 你好
 *      日本语 こにちは
 *      英语 Nice to meet you
 *
 * @author wangxianshou
 *
 */
public class LeiFeng {

	public void sweep() {
		System.out.println("sweep");
	}

	public void wash() {
		System.out.println("wash");
	}

	public void buyRice() {
		System.out.println("buyRice");
	}

}
