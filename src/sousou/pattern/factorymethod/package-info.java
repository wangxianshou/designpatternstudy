/**
 * 工厂方法模式
 * Define an interface for creating anobject, but let subclasses decide which class to
 * instantiate. Factory methodlets a class defer instantiation to subclasses.
 *
 * 定义一个用于创建对象的接口，让子类决定将哪一个类实例化，FactoryMethod使一个类的实例化延迟到其子类。
 */
/**
 * @author wangxianshou
 *
 */
package sousou.pattern.factorymethod;