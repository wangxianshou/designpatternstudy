package sousou.pattern.factorymethod;

public class GoGoGo {

	public static void main(String[] args) {
		IFactory factory = new UndergraduateFactory();
		LeiFeng lf = factory.createLeiFeng();
		lf.sweep();
		//
		factory = new VolunteerFactory();
		lf = factory.createLeiFeng();
		lf.buyRice();
	}

}
