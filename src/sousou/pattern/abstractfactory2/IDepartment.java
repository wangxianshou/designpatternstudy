package sousou.pattern.abstractfactory2;

public interface IDepartment {
	void insert(Department department);
	Department getDepartment(int id);


}
