package sousou.pattern.abstractfactory2;

public interface IUser {

	void insert(User user);
	User getUser(int id);

}
