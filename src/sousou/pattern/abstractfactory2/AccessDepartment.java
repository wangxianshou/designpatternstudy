package sousou.pattern.abstractfactory2;

public class AccessDepartment implements IDepartment {

	@Override
	public void insert(Department department) {
		System.out.println("AccessDepartment.insert");

	}

	@Override
	public Department getDepartment(int id) {
		System.out.println(String.format("AccessDepartment.getDepartment by ID[%d]", id));
		return null;
	}
}
