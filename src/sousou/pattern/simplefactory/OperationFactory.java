package sousou.pattern.simplefactory;

public class OperationFactory {

	public static Operation createOperate(String operate) {
		Operation oper = null;
		//增加的时候，违背开放-封闭原则-->通过工厂模式来改善
		switch(operate) {
		case "+":
			oper = new OperationAdd();
			break;
		case "-":
			oper = new OperationSub();
			break;
		case "*":
			oper = new OperationMul();
			break;
		case "/":
			oper = new OperationDiv();
			break;
		}
		return oper;
	}

}
