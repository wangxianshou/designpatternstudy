package sousou.pattern.simplefactory;

public class OperationMul extends Operation {
	@Override
	public double getResult() {
		double result = 0;
		result = this.getNumberA() * this.getNumberB();
		return result;
	}
}
