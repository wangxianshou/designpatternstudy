package sousou.pattern.simplefactory;

public class OperationSub extends Operation {
	@Override
	public double getResult() {
		double result = 0;
		result = this.getNumberA() - this.getNumberB();
		return result;
	}
}
