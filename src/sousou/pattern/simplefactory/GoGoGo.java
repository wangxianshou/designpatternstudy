package sousou.pattern.simplefactory;

public class GoGoGo {

	public static void main(String[] args) {
		Operation oper = OperationFactory.createOperate("+");
		oper.setNumberA(2);
		oper.setNumberB(5);
		System.out.println(oper.getResult());
		//
		oper = OperationFactory.createOperate("-");
		oper.setNumberA(2);
		oper.setNumberB(5);
		System.out.println(oper.getResult());
		//
		oper = OperationFactory.createOperate("*");
		oper.setNumberA(2);
		oper.setNumberB(5);
		System.out.println(oper.getResult());
		//
		oper = OperationFactory.createOperate("/");
		oper.setNumberA(2);
		oper.setNumberB(5);
		System.out.println(oper.getResult());

	}

}
