package sousou.pattern.simplefactory;

public class OperationDiv extends Operation {
	@Override
	public double getResult() {
		double result = 0;
		if(this.getNumberB() == 0 ) {
			System.out.println("除数不能为0！");
		}else {
			result = this.getNumberA() / this.getNumberB();
		}
		return result;
	}
}
