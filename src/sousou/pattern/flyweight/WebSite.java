package sousou.pattern.flyweight;

public abstract class WebSite {

	public abstract void use(User user);

}
