package sousou.pattern.flyweight;

import java.util.Hashtable;
/**
 * 网站工厂
 * @author wangxianshou
 *
 */
public class WebSiteFactory {
	private Hashtable<String, WebSite> flyweights = new Hashtable<>();
	/**
	 * 获得网站分类
	 * @param key
	 * @return
	 */
	public WebSite getWebSiteCategory(String key) {
		if(!flyweights.contains(key)) {
			flyweights.put(key, new ConcreteWebSite(key));
		}
		return flyweights.get(key);
	}
	/**
	 * 获得网站分类总数
	 * @return
	 */
	public int getWebSiteCount() {
		return flyweights.size();
	}

}
