package sousou.pattern.command;

public class Waiter {
	private Command command;
	public void setOrder(Command command) {
		System.out.println(String.format("setOrder:%s", command.getClass().getSimpleName()));
		this.command = command;
	}

	public void notifyExcute() {
		command.excuteCommand();
	}

}
