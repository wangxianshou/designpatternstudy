package sousou.pattern.command;

import java.util.ArrayList;
import java.util.List;

public class WaiterEx {
	private Command command;
	public List<Command> orders = new ArrayList<>();
	public void setOrder(Command command) {
		if(command.getClass().getSimpleName().equals("BakeChickenWingCommand")) {
			System.out.println("服务员：鸡翅没有了，请点别的烧烤");
		}else {
			System.out.println(String.format("setOrder:%s", command.getClass().getSimpleName()));
			this.command = command;
		}

	}
	public void cancelOrder(Command command) {
		orders.remove(command);
		System.out.println(String.format("cancelOrder:%s", command.getClass().getSimpleName()));
	}

	public void notifyExcute() {
		for(Command it : orders) {
			it.excuteCommand();
		}
	}
}
