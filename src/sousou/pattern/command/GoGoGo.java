package sousou.pattern.command;

public class GoGoGo {

	/**
	 * 1.并不是用户点一个菜，服务员就通知做一个
	 * 2.如果鸡翅没有了，不应该是客户来判断是否还有，应该由服务员或者烤肉串师傅决定
	 * 3.客户到底点了哪些烧烤和饮料，需要记录
	 * 4.因为肉串点的太多而考虑取消一些还没有制作的肉串
	 * @param args
	 */
	public static void main(String[] args) {
		//开店前准备
		Barbecuer boy = new Barbecuer();
		Command bakeMutton1 = new BakeMuttonCommand(boy);
		Command bakeMutton2 = new BakeMuttonCommand(boy);
		Command bakeChickenWing1 = new BakeChickenWingCommand(boy);
		//
		Waiter girl = new Waiter();
		girl.setOrder(bakeMutton1);
		girl.notifyExcute();
		//
		girl.setOrder(bakeMutton2);
		girl.notifyExcute();
		//
		girl.setOrder(bakeChickenWing1);
		girl.notifyExcute();

		System.out.println("================================");
		WaiterEx girlEx = new WaiterEx();
		girlEx.setOrder(bakeMutton1);
		girlEx.setOrder(bakeMutton2);
		girlEx.setOrder(bakeChickenWing1);
		girlEx.notifyExcute();



	}

}
