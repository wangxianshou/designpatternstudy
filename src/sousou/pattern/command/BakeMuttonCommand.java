package sousou.pattern.command;

public class BakeMuttonCommand extends Command {

	public BakeMuttonCommand(Barbecuer receiver) {
		super(receiver);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	@Override
	public void excuteCommand() {
		this.receiver.bakeMutton();

	}

}
