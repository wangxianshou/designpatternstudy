package sousou.pattern.command;
/**
 * 烤肉串师傅
 * @author wangxianshou
 *
 */
public class Barbecuer {
	public void bakeMutton() {
		System.out.println("烤羊肉串");
	}

	public void bakeChickenWing() {
		System.out.println("烤鸡翅");
	}

}
