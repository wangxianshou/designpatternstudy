package sousou.pattern.delegate;

import java.text.SimpleDateFormat;
import java.util.Date;

public class WatchCartoonListener implements IAction {
	SimpleDateFormat sdf=new SimpleDateFormat("yyyy年MM月dd日HH时mm分ss秒");
	@Override
	public void start(Date date) {
		System.out.println(String.format("WatchCartoonListener.start:%s", sdf.format(date)));
	}

	@Override
	public void stop(Date date) {
		System.out.println(String.format("WatchCartoonListener.stop:%s", sdf.format(date)));
	}



}
