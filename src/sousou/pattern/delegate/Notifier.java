package sousou.pattern.delegate;

/**
 * 通知者的抽象类
 * @author wangxianshou
 *
 */
public abstract class Notifier {
	public EventHandler getEventHandler() {
		return eventHandler;
	}

	public void setEventHandler(EventHandler eventHandler) {
		this.eventHandler = eventHandler;
	}

	private EventHandler eventHandler = new EventHandler();

	public abstract void addListener(Object object,String methodName,Object... args);

	public abstract void notifyX();


}
