package sousou.pattern.delegate;

import java.util.ArrayList;
import java.util.List;


/**
 * 事件的 处理者
 * @author wangxianshou
 *
 */
public class EventHandler {
	private List<Event> eventList;
	public EventHandler() {
		eventList = new ArrayList<>();
	}
	//添加某个对象要执行的事件，及需要的参数
	public void addEvent(Object object,String methodName,Object... args) {
		eventList.add(new Event(object,methodName,args));
	}
	//通知所有的对象执行指定的事件
    public void notifyX() throws Exception{
        for(Event e : eventList){
            e.invoke();
        }
    }

}
