package sousou.pattern.delegate;

import java.text.SimpleDateFormat;
import java.util.Date;

public class WatchingNBAListener implements IAction {

	SimpleDateFormat sdf=new SimpleDateFormat("yyyy年MM月dd日HH时mm分ss秒");
	@Override
	public void start(Date date) {
		System.out.println(String.format("WatchingNBAListener.start:%s", sdf.format(date)));
	}

	@Override
	public void stop(Date date) {
		System.out.println(String.format("WatchingNBAListener.stop:%s", sdf.format(date)));
	}

}
