/**
 * java里的回调是什么？
 * 这里讲解的很详细，http://blog.csdn.net/veryitman/article/details/6937468
 * 就是A类中调用B类中的某个方法c，然后B类中c反过来调用A类中的方法d，d这个方法就叫回调方法。
 * 其实strategy**、template**、observer**、visitor**模式全部都是回调的不同应用。
 * 简单来说就是本来可以写死在一起的代码给拆开来，让其中一块保持原有的流程，并在流程中挖出一些空，
 * 让另一块代码作为参数传进来在流程中合适的地方被调用，也就是UML里所说的聚合。
 * 作者：iceqing
 * 链接：https://www.jianshu.com/p/d64cfad99689
 * 来源：简书
 * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 *
 * java中没有委托，但是java中有个委托事件的模型。
 *
 * 观察者模式与事件委托,异曲同工
 * https://www.jianshu.com/p/d64cfad99689
 */
/**
 * @author wangxianshou
 *
 */
package sousou.pattern.delegate;