package sousou.pattern.delegate;

import java.lang.reflect.Method;

public class Event {

	private Object object;
	private String methodName;
	private Object[] params;
	private Class[] paramTypes;

	public Event(Object object,String methodName,Object... args) {
		this.object = object;
		this.methodName = methodName;
		this.params =args;
		this.createParamTypes(args);

	}

	private void createParamTypes(Object[] params) {
		this.paramTypes = new Class[params.length];
		for(int i=0;i<params.length;i++) {
			this.paramTypes[i] = params[i].getClass();
		}
	}

	public Object getObject() {
		return this.object;
	}

	public void invoke() throws Exception {
		Method method = object.getClass().getMethod(this.methodName, this.paramTypes);
		if(null == method) {
			return;
		}
		method.invoke(this.object, this.params);
	}

}
