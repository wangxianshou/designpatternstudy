package sousou.pattern.delegate;

import java.util.Date;

public interface IAction {

	void start(Date date);
	void stop(Date date);

}
