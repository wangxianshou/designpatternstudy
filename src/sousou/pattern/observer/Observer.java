package sousou.pattern.observer;

/**
 * 抽象观察者
 * @author wangxianshou
 *
 */
public abstract class Observer {

	protected String name;
	protected ISubject sub;

	public Observer(String name,ISubject sub) {
		this.name = name;
		this.sub = sub;
	}
}
