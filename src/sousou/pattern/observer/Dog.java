package sousou.pattern.observer;

public class Dog extends Animal implements IUpdate {

	public Dog(String name, ISubject sub) {
		super(name, sub);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	@Override
	public void update(String info) {
		System.out.println(String.format("%s %s 停止啃骨头，继续工作", info,name));

	}

	@Override
	public String getObserverName() {
		// TODO 自動生成されたメソッド・スタブ
		return this.name;
	}

}
