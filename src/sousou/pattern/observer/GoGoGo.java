package sousou.pattern.observer;

public class GoGoGo {

	public static void main(String[] args) {
		System.out.println("====多亏前台小姐姐，逃过一劫========================");
		//前台通知 or 老板直接怼你面前
		Secretary qiantai = new Secretary();
		Boss  laoban = new Boss();
		//看股票同事们
		StockObserver tongshiA = new StockObserver("Tony",qiantai);
		StockObserver tongshiB = new StockObserver("Jhon",qiantai);
		//看NBA
		NBAObserver tongshiC = new NBAObserver("Mary",qiantai);
		NBAObserver tongshiD = new NBAObserver("Lisa",qiantai);
		//公司的大黄狗
		Dog xiaohei = new Dog("xiaohei",laoban);
		//前台记下了同事们
		qiantai.attach(tongshiA);
		qiantai.attach(tongshiB);
		qiantai.attach(tongshiC);
		qiantai.attach(tongshiD);
		//通知外(Mary)
		qiantai.detach(tongshiC);
		qiantai.setAction("boss come back !!!");
		qiantai.notifyInfo();
		System.out.println("====被老板抓现行=================================");
		laoban.attach(tongshiA);
		laoban.attach(tongshiB);
		laoban.attach(tongshiC);
		laoban.attach(tongshiD);//人
		laoban.attach(xiaohei);//狗{针对接口编程}
		laoban.setAction("wan de hen hao ya !!!");
		laoban.notifyInfo();



	}

}
