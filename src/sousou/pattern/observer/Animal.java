package sousou.pattern.observer;

public class Animal {
	protected String name;
	protected ISubject sub;

	public Animal(String name,ISubject sub) {
		this.name = name;
		this.sub = sub;
	}
}
