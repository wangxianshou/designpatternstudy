package sousou.pattern.observer;

/**
 * 通知者接口
 * @author wangxianshou
 *
 */
public interface ISubject {
	void attach(IUpdate observer);
	void detach(IUpdate observer);
	void notifyInfo();

}
