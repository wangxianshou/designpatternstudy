package sousou.pattern.observer;
/**
 * 看股票观察者
 * @author wangxianshou
 *
 */
public class StockObserver extends Observer implements IUpdate{


	public StockObserver(String name, ISubject sub) {
		super(name, sub);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public void update(String info) {
		System.out.println(String.format("%s %s 关闭股票行情，继续工作", info,name));
	}

	@Override
	public String getObserverName() {
		return this.name;
	}
}
