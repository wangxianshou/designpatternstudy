package sousou.pattern.observer;

/**
 * 老板（具体通知者）
 * @author wangxianshou
 *
 */
public class Boss extends Worker{

	@Override
	public void notifyInfo() {
		for(IUpdate it : observers) {
			String who = it.getObserverName();
			//个别对待
			switch(who) {
			case "xiaohei":
				it.update("宰了吃狗肉");
				break;
			case "Mary":
				//权当没看见
				break;
			case "Lisa":
				it.update("work more,shao wan hui");
				break;
			default://训斥
				it.update(this.getAction());
				break;
			}
		}
	}


}
