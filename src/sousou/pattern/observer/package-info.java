/**
 * 观察者模式【当一个对象的改变需要同时改变其他对象的时候，适用】
 * Define a one-to-many dependency between objects so that when one object changes
 * state all its dependents are notified and updated automatically.
 *
 * 定义对象间的一种一对多的依赖关系，以便当一个对象的状态发生改变时，所有依赖于它的对象都得到通知并自动刷新。
 *
 * 抽象通知者接口  ---- 抽象观察者接口
 * 具体
 *    老板                               看股票(建立和抽象通知者的关系)
 *    前台                               看NBA(建立和抽象通知者的关系)
 *
 *将一个系统分割成一系列相互协作的类有一个很不好的副作用，那就是需要维护相关对象间的一致性，
 *我们不希望为了维持一致性而使各类紧密耦合，这样会给维护，扩展和重用都带来不便。
 *
 *观察者模式的关键对象是主题Subject和观察者Observer
 *一个Subject可以有任意数目的依赖它的观察者，
 *一旦Subject的状态发生了改变，所有的Observer都可以得到通知。
 *Subject发出通知时并不需要谁是它的观察者，也就是说，具体观察者是谁，它根本不需要知道。
 *而任何一个具体观察者不知道也不需要知道其他观察者的存在。
 *
 *解除耦合，让耦合的双方都依赖于抽象，而不是依赖于具体，从而使得各自的变化都不会影响另一边的变化。
 *
 *具体的观察者完全有可能是风马牛不相及的类，但它们都需要根据通知者的通知来做出update的操作，
 *所以让它们都实现接口是比较好的
 *
 */
/**
 * @author wangxianshou
 *
 */
package sousou.pattern.observer;