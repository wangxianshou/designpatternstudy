package sousou.pattern.observer;

/**
 * 看NBA观察者
 * @author wangxianshou
 *
 */
public class NBAObserver extends Observer implements IUpdate{

	public NBAObserver(String name, ISubject sub) {
		super(name, sub);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	@Override
	public void update(String info) {
		System.out.println(String.format("%s %s 关闭NBA，继续工作", info,name));

	}

	@Override
	public String getObserverName() {
		return this.name;
	}

}
