package sousou.pattern.observer;

public interface IUpdate {

	void update(String info);
	String getObserverName();

}
