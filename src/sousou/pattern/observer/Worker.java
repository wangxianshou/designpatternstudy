package sousou.pattern.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * 老板和前台都是工作人员
 * @author wangxianshou
 *
 */
public class Worker implements ISubject{

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	private String action;

	//观察者列表
	protected List<IUpdate> observers = new ArrayList<>();

	@Override
	public void attach(IUpdate observer) {
		observers.add(observer);
	}

	@Override
	public void detach(IUpdate observer) {
		observers.remove(observer);

	}

	@Override
	public void notifyInfo() {
		for(IUpdate it : observers) {
			it.update(this.getAction());
		}
	}
}
